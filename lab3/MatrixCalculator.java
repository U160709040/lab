public class MatrixCalculator {

   public static void main(String[] args) {
   int[][] matrixA = {{6, 8, 2}, {9, 5, 11}, {7, 2, 5}};
   int[][] matrixB = {{4, 6, 3}, {5, 8, 1}, {6, 6, 7}};
   int sum[][] =FindSum(matrixA, matrixB); /* we didn't define sum here, so that's why we define again */ 
   printMatrix(FindSum(matrixA, matrixB));
}

    public static int[][] FindSum(int[][] m1, int[][] m2) {
    int[][] sum= null; /* null is a reference type and its value is the only reference    value which doesn't refer to any object */ 
    sum = new int [3][3]; /* we give the array's size */ 
    for(int i = 0; i < 3; i++) {
          for(int j = 0; j < 3; j++) {

              sum[i][j] = m1[i][j] + m2[i][j];
        }
      }
    return sum; }


  public static void printMatrix(int[][] matrix) {
   for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        System.out.print(" "+matrix[i][j]);
      }
       System.out.println(); /* with this we print line by line the matrixes */
    }
  }
}


