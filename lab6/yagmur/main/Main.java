package yagmur.main;
import yagmur.shapes.Circle;

import java.util.ArrayList;
// import java.lang.*; important classes by default 
// ArrayList has no limit like Arrays
public class Main{

	public static void main(String[] args) {
		Circle circle1 = new Circle(6);
		Circle circle2 = new Circle(10);
		Circle circle3 = new Circle(3);
		
         ArrayList<Circle> array = new ArrayList<Circle>();
         
         array.add(circle1);
         array.add(circle2);
         array.add(circle3);
       for (int i = 0; i < array.size() ; i++) { 
    	   System.out.println("The area of the circle" + (i+1)+ 
    			   "  " +  array.get(i).area());
       }
	}

}
