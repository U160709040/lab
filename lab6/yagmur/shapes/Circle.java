package yagmur.shapes;

public class Circle {
      public int radius;
      
      public Circle() {
    	  
      }
       public Circle (int radius) { 
    	   this.radius = radius;
       }
       
       public double area() {
    	   return 3.14 * radius * radius; 
       }
}

       
