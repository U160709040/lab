package yagmur.shapes3d;

import yagmur.shapes.Square;

public class Cube extends Square {
      int Side;
	public Cube(int a) {
		super();
		Side = a;
	}
    @Override
	public String toString() {
		return ( "Side of the cube  " + Side);
	}
	
	@Override
	public double area() {
		return  ( Side * Side ) * 6;
	}
	
	public double volume() {
		return Side * Side * Side;
	}
}
