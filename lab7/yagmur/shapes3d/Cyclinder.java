package yagmur.shapes3d;

import yagmur.shapes.Circle;

public class Cyclinder extends Circle {
       
	int height;
	int radius;
	public Cyclinder(int radius, int height) {
		super(radius); 
		
		this.radius = radius; 
		this.height = height;
		
	}
@Override
	public String toString() {
		return ("Radius of the cyclinder " + radius + " and height of the cyclinder  " + height);
	}
	public double area() {
		return (2 * radius * height * Math.PI) + (2 * super.area());
	}
	
	public double volume() {
		return ( super.area() * height );
	}
}
