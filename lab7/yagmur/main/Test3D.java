package yagmur.main;
import yagmur.shapes3d.*;
public class Test3D {

	public static void main(String[] args) {
		
		
         Cyclinder c1 = new Cyclinder(10, 5);
         Cube cube1 = new Cube(12);
         
         
         System.out.println(c1);
         System.out.println(cube1.toString());
         System.out.println("Area of the cyclinder  "  + c1.area()  );
	     System.out.println("Volume of the cyclinder  " + c1.volume());
	     System.out.println("Area of the cube  "  + cube1.area()  );
	     System.out.println("Volume of the cube  " + cube1.volume());
	} 

}
