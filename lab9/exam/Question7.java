package exam;

public class Question7 {

	public static void main(String[] args) {
          
		System.out.println(powerN(3, 2));
	}
	
	
	private static int powerN(int num, int pow) {
		if ( pow == 0 ) {
		 return 1;	
		}
		
		return num * powerN(num, pow-1);
	}

}
