public class GCDLoop{

public static void main(String [] args) {
    int a = Integer.parseInt(args[0]);
    int b = Integer.parseInt(args[1]);
    int result = (GCD(a,b));
    System.out.println(result);

     } // main

   
    public static int GCD(int a, int b) {
        while(a!=0 && b!=0) // until either one of them is 0
         {
          int c = b; 
          b = a % b ; // b becomes remainder
          a = c;
           }
         return a+b; // either one is 0, so return the non-zero value

          } // GCD 
} // class 



