public class GCDRec{

 

 public static void main(String[] args) {
     int a = Integer.parseInt(args[0]);
     int b = Integer.parseInt(args[1]);
     int result = GCDR(a,b);
     System.out.println(result);
           } // main




 public static int GCDR(int a, int b) {

     if (b == 0 ) {   // base case
         return a+b; } //b becomes 0 and the sum is equal the GCD.
         
     return GCDR(b, a % b); 
   

} // GCDR

 } // class
